#!/bin/bash

set -e

wget https://gitee.com/dcyh/shell/raw/master/supdev_init
if [ ! -d "/home/supdev" ]; then
    mkdir /home/supdev
fi

# 清除旧配置
if [ -f "/home/supdev/.bash_init" ];then
    if [ -f "/home/supdev/.bash_init.bak" ];then
        rm -f /home/supdev/.bash_init
    else
        mv /home/supdev/.bash_init /home/supdev/.bash_init.bak
    fi
fi

mv supdev_init /home/supdev/.bash_init

echo ". /home/supdev/.bash_init">>/root/.bashrc
echo ". /home/supdev/.bash_init">>/home/supdev/.bash_profile

chmod 777 /home/supdev/.bash_init
source /home/supdev/.bash_init

# 修改 vi 制表符长度
if [ -f "/etc/vim/vimrc" ];then
    echo "set tabstop=4">>/etc/vim/vimrc
fi

if [ -f "/etc/virc" ];then
    echo "set tabstop=4">>/etc/virc
fi

if [ -f "/var/app/readme" ];then
    echo "# wget -qO- https://gitee.com/dcyh/shell/raw/master/supdev_install.sh | sudo bash; source /home/supdev/.bash_init">/var/app/readme
fi

# 项目配置
touch /var/app/.bash_profile